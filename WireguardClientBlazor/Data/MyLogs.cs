using System;

namespace WireguardClientBlazor.Data
{
    public static class MyLogs
    {
        public static void LogError(string functionName, string message)
        {
            string log = "";
            log += "++" + "\n" +
                   "Error:"+ message +"\n" +
                   $"Function Name : {functionName}" + "\n"+
                   "--";
            Console.WriteLine(log);
            AppendLog(log);
        }

        static void AppendLog(string text)
        {
            _logs += text + "\n";
        }

        private static string _logs;
        public static string Logs => _logs;
    }
}