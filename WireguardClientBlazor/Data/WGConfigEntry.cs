namespace WireguardClientBlazor.Data
{
    public class WGConfigEntry
    {
        public string Name { set; get; }
        public string Path { set; get; }
        public bool IsOnline { set; get; }
        // public string Configuration { set; get; } removed as it will read the file from disk no need to hold it
        public long BytesSent { set; get; }
        public long BytesReceived { set; get; }
        public float RX { set; get; }
        public float TX { set; get; }
        public float Latency { set; get; }

    }
}