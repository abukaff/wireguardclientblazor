using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

namespace WireguardClientBlazor.Data
{
    // its not Variables only, it contains functions as well, i could not think of a better name
    public static class GlobalVariables
    {
        public static string DefaultConfigurationsPath = "/etc/wireguard/";
        public static bool bRequirementsSatisfied = true;

        static GlobalVariables()
        {
#if DEBUG
            DefaultConfigurationsPath = RunCommand("echo $HOME/test/");
            DefaultConfigurationsPath = DefaultConfigurationsPath.Replace("\n", string.Empty);
#endif
            bRequirementsSatisfied = CheckAllRequirements();
        }

        public static bool GetStatus(string interfaceName)
        {
            string command = "ip -o link show | awk -F': ' '{print $2}' | grep " + $"\"\\<{interfaceName}\\>\"";
            var temp = RunCommand(command);
            bool result = !string.IsNullOrEmpty(temp);
            return result;
        }

        public static string GetIPAddress(string interfaceName)
        {
            string command = "/sbin/ifconfig " + interfaceName + " | grep 'inet ' | cut -d: -f2 | awk '{print $2}'";
            var result = RunCommand(command);
            return result;
        }

        public static string GetConfigurationFromFile(string Path)
        {
            string result = "";
            try
            {
                result = File.ReadAllText(Path);
            }
            catch (Exception err)
            {
                result = "Error reading file";
                var functionName = MethodBase.GetCurrentMethod().Name;
                MyLogs.LogError(functionName, err.ToString());
            }

            return result;
        }

        public static List<string> GetConfigurationFiles(string path)
        {
            List<string> result = new List<string>();
            var tempResult = RunCommand($"ls {GlobalVariables.DefaultConfigurationsPath} | grep .conf$").Split('\n');
            result = tempResult.ToList().Where(s => !string.IsNullOrWhiteSpace(s)).Distinct().ToList();
            return result;
        }

        public static bool Connect(string configName)
        {
            bool isConnected = false;
            try
            {
                string command = "wg-quick up " + configName;
                var result = RunCommand(command);
                Console.WriteLine(result);
                isConnected = GetStatus(configName);
                if (!isConnected)
                    throw new Exception(
                        $"Error while trying to connect using [{configName}], Please check the configuration file");
            }
            catch (Exception err)
            {
                var functionName = MethodBase.GetCurrentMethod().Name;
                MyLogs.LogError(functionName, err.Message);
            }

            return isConnected;
        }

        public static bool Disconnect(string configName)
        {
            bool isConnected = false;
            string command = "wg-quick down " + configName;
            var result = RunCommand(command);
            Console.WriteLine(result);
            isConnected = GetStatus(configName);
            return isConnected;
        }

        public static bool UpdateConfiguration(WGConfigEntry wgConfigEntry, string newConfig)
        {
            bool isSuccess = false;
            try
            {
                File.WriteAllText(wgConfigEntry.Path, newConfig);
                isSuccess = true;
            }
            catch (Exception err)
            {
                var functionName = MethodBase.GetCurrentMethod().Name;
                MyLogs.LogError(functionName, err.Message);
            }

            return isSuccess;
        }

        public static string RunCommand(string cmd)
        {
            Console.WriteLine(cmd);
            var escapedArgs = cmd.Replace("\"", "\\\"");
            var process = new Process()
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "/bin/bash",
                    Arguments = $"-c \"{escapedArgs}\"",
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                }
            };
            process.Start();
            string result = process.StandardOutput.ReadToEnd();
            // Wait for exit causes a deadlock for some reason
            //process.WaitForExit();

            return result;
        }

        public static bool UpdateConfiguration(string name, string config)
        {
            bool isSucess = false;
            WGConfigEntry wgConfigEntry = new WGConfigEntry
            {
                Name = name,
                Path = DefaultConfigurationsPath + name + ".conf"
            };
            
            isSucess = UpdateConfiguration(wgConfigEntry, config);
            return isSucess;
        }

        public static bool DeleteConfig(string path)
        {
            bool isSccess = false;
            try
            {
                File.Delete(path);
                isSccess = true;
            }
            catch (Exception err)
            {
                var functionName = MethodBase.GetCurrentMethod().Name;
                MyLogs.LogError(functionName, err.Message);
            }

            return isSccess;
        }

        public static bool CheckRequirementSudo()
        {
            bool isSuccess = true;
            var result = RunCommand("id -u").Trim();
            if (result != "0")
                isSuccess = false;
            return isSuccess;
        }

        public static bool CheckRequirementWG()
        {
            bool isSuccess = true;
            var resultBIN = RunCommand("ls /usr/bin/ | grep wg-quick").Trim();
            var resultSBIN = RunCommand("ls /usr/sbin/ | grep wg-quick").Trim();
            if (resultBIN != "wg-quick")
                if (resultSBIN != "wg-quick")
                    isSuccess = false;
            return isSuccess;
        }

        public static bool CheckRequirementIFConfig()
        {
            bool isSuccess = true;
            var resultBIN = RunCommand("ls /usr/bin/ | grep ifconfig").Trim();
            var resultSBIN = RunCommand("ls /usr/sbin/ | grep ifconfig").Trim();
            if (resultBIN != "ifconfig")
                if (resultSBIN != "ifconfig")
                    isSuccess = false;
            return isSuccess;
        }

        public static bool CheckAllRequirements()
        {
            return
                CheckRequirementSudo() &&
                CheckRequirementWG() &&
                CheckRequirementIFConfig();
        }
    }
}