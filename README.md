# Wireguad Client Blazor - Linux
![visitors](https://visitor-badge.deta.dev/badge?page_id=abukaff.wireguardblazor&left_color=#4A5454&right_color=#5391C9)

[[_TOC_]]

## What is it ?

An unoffical front end for Wireguard client on Linux


## How to run it
Binaries
* [Release](https://gitlab.com/abukaff/wireguardclientblazor/-/releases)

Requirement
* Wireguard to be already installed
* ifconfig aka net-tools
* If you dont want to run the selfcontained version you need .Net 6.0

An installation script is included that would install the application under opt/ and create the .desktop shortcut for you
```bash
sudo sh Install.sh
```
then simply search Wireguard GUI under your DE and you should find it

Note: Desktop uses pkexec to request elevation aka sudo as wg-quick commands require sudo to execute

### Manual
Just run the following on terminal in the extracted files folder
```bash
sudo ./WireguardClientBlazor
```
![WireguardBlazor](https://gitlab.com/abukaff/wireguardclientblazor/-/raw/develop/WireguardClientBlazor/wwwroot/Images/GUI-Screenshot.png)

## Uninstall
Uninstall script is included
```bash
sudo sh Uninstall.sh
```


