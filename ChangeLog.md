#Change Log

[[_TOC_]]

### v0.0.4
```
- Added Requirements Check
```
### v0.0.3
```
- Changed from Radzen to MudBlazor
- DARK MODE ! who does not want a dark mode ?
- Search
- Fixed the check status to find exact match for more accurate results
- Added app bar and a drawer for future uses
```
### v0.0.2
```
- Auto refresh functionality to display throughput
```
### v0.0.1
```
- Initial release 
```